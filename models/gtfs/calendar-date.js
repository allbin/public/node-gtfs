const mongoose = require('mongoose');

const calendarDateSchema =  new mongoose.Schema({
  agency_key: {
    type: String,
    required: true,
  },
  service_id: {
    type: String,
    required: true
  },
  date: {
    type: Number,
    required: true
  },
  exception_type: {
    type: Number,
    required: true,
    min: 1,
    max: 2
  },
  holiday_name: String
});
calendarDateSchema.index({service_id: 1,date:1},{unique: true});

module.exports = mongoose.model('CalendarDate',calendarDateSchema);
