const mongoose = require('mongoose');

const agencySchema =  new mongoose.Schema({
  agency_key: {
    type: String,
    required: true,
  },
  agency_id: {
    type: String,
    unique: true
  },
  agency_name: {
    type: String,
    required: true
  },
  agency_url: {
    type: String,
    required: true
  },
  agency_timezone: {
    type: String,
    required: true
  },
  agency_lang: String,
  agency_phone: String,
  agency_fare_url: String,
  agency_email: String,
  agency_bounds: {
    sw: {
      type: Array,
      index: '2dsphere'
    },
    ne: {
      type: Array,
      index: '2dsphere'
    }
  },
  agency_center: {
    type: [Number],
    index: '2dsphere'
  },
  date_last_updated: Number
});

module.exports = mongoose.model('Agency',agencySchema);
